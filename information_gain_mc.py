import numpy as np
import logging

logger = logging.getLogger(__name__)

def calc_pmin(f, Nb):
    if len(f.shape) == 3:
        f = f.reshape(f.shape[0], f.shape[1] * f.shape[2])
    mins = np.argmin(f, axis=0)
    c = np.bincount(mins)
    min_count = np.zeros((Nb,))
    min_count[:len(c)] += c
    pmin = (min_count / f.shape[1])[:, None]
    pmin[np.where(pmin < 1e-70)] = 1e-70
    return pmin
    
# actual function     
def joint_pmin(Mb, Vb, Nf):
    Nb = Mb.shape[0]
    F = np.random.multivariate_normal(mean=np.zeros(Nb), cov=np.eye(Nb), size=Nf)
    if np.any(np.isnan(Vb)):
        raise Exception(Vb)
    try:
        cVb = np.linalg.cholesky(Vb)

    except np.linalg.LinAlgError:
        try:
            cVb = np.linalg.cholesky(Vb + 1e-10 * np.eye(Vb.shape[0]))
        except np.linalg.LinAlgError:
            try:
                cVb = np.linalg.cholesky(Vb + 1e-6 * np.eye(Vb.shape[0]))
            except np.linalg.LinAlgError:
                cVb = np.linalg.cholesky(Vb + 1e-3 * np.eye(Vb.shape[0]))
    f = np.add(np.dot(cVb, F.T).T, Mb).T
    pmin = calc_pmin(f, Nb)
    return pmin.T