#! /bin/python
import unittest
import numpy as np
from information_gain import joint_min
from information_gain_mc import joint_pmin


class test_functions(unittest.TestCase):
    def setUp(self):
        self.mu_rnd = np.random.randn(15)
        self.A_rnd = np.random.random((15, 15))

    def test_comparison_exp(self):
        Nf = 100
        mu = np.array([ 0.42656271, 0.97610833])
        A = np.array([[ 0.14377992 , 0.59048246],
                      [ 0.33624042 , 0.90143748]])

        cov = np.dot(A,np.transpose(A))
        min_one = joint_min(mu,cov)
        min_two = joint_pmin(mu,cov,Nf)
        res = np.allclose(np.exp(min_one),min_two, (1/np.sqrt(Nf)))
        
        self.assertFalse(res)
        
    def test_comparison_log(self):
        Nf = 100
        mu = np.array([ 0.42656271, 0.97610833])
        A = np.array([[ 0.14377992 , 0.59048246],
                      [ 0.33624042 , 0.90143748]])
                      
        cov = np.dot(A,np.transpose(A))
        min_one = joint_min(mu, cov)
        min_two = joint_pmin(mu,cov, Nf)
        res = np.allclose(min_one,np.log(min_two), (1/np.sqrt(Nf)))
        
        self.assertFalse(res)
        
    def test_comparison_rnd_exp(self):
        Nf = 1000
        mu = self.mu_rnd
        A = self.A_rnd
        cov = np.dot(A,np.transpose(A))
        min_one = joint_min(mu,cov)
        min_two = joint_pmin(mu,cov,Nf)
        res = np.allclose(np.exp(min_one),min_two, (1/np.sqrt(Nf)))
        
        self.assertFalse(res)
        
    def test_comparison_rnd_log(self):
        Nf = 1000
        mu = self.mu_rnd
        A = self.A_rnd
        cov = np.dot(A,np.transpose(A))
        min_one = joint_min(mu, cov)
        min_two = joint_pmin(mu,cov, Nf)
        res = np.allclose(min_one,np.log(min_two), (1/(np.sqrt(Nf))))
        
        self.assertFalse(res)
        
    
unittest.main()